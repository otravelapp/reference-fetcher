#!  Built on Python 2.7  
#   Reads each line of new.txt file and queries it on google scholars for reference
#   saves fetched reference in ref0.txt
#   There is random time delay between queries to imitate human behaviour to google servers
#   Requirement for Automatic IP switching:
#       1. Computer should be using wifi hotspot of an android device
#       2. The android device should be connected to computer via ADB
#       3. ADB should be added in environment variables or function restartdata()
#           should be modified with specific adb.exe location 


import scholarpy
import time
import random
import winsound
from subprocess import call

#   Change System Coding to UTF8 to handle text properly
# sys.setdefaultencoding() does not exist, here!
import sys
reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')


outputfilename='ref'
outputfileext='.txt'

startref=1
totalrefsinfile=30
currentref=startref

outputfile=outputfilename+'0'+outputfileext
fout = open(outputfile,'w')

def restartdata():
    call("adb shell svc data disable" , shell=True)
    time.sleep(15)
    call("adb shell svc data enable" , shell=True)
    time.sleep(15)
    return 0

def makeBeep():
    Freq = 2500 # Set Frequency To 2500 Hertz
    Dur = 1000 # Set Duration To 1000 ms == 1 second
    winsound.Beep(Freq,Dur)

def update_file_name(curref):
    global fout
    global totalrefsinfile
    fout.close()
    makeBeep()
#     variable = raw_input('file closed, input something for now one ')
    restartdata()
    filenum=curref/totalrefsinfile+1
    outputfile=outputfilename+'_'+str(filenum).zfill(3)+outputfileext
    fout=open(outputfile, 'w')
    print outputfile
    
    
currentreadref=0
def service_func():
    #   Open file to read references
    f = open('new.txt')
    global currentref
    global currentreadref
    global startref
    for line in iter(f):
        currentreadref+=1
        if currentreadref<startref:
            continue
        
        if currentref%totalrefsinfile==0:
            update_file_name(currentref)
        reference=line.decode('utf-8').rstrip('\n')
        
        print 'Ref No. ', currentref
        print reference
        fout.write('Reference No. '+ str(currentref)+ '\n')
        
        fout.write('String: '+ reference.encode('utf8') +'\n')
        
        details=scholarpy.main(['-c', '1', '--phrase', reference])
                        
        fout.write(str(details)+'\n**************************************\n\n')
#         print str(me)
        delaytime=random.randint(15,25)
        time.sleep(delaytime)
        currentref+=1
        
    f.close()
    fout.close()
    
    

if __name__ == '__main__':
    # service.py executed as script
    # do something
    service_func()
    

 